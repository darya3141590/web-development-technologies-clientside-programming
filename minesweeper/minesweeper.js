document.addEventListener('DOMContentLoaded', e => {

  const mw = document.querySelector('.mw');
  const width = 12;
  const height = 12;
  const bombCount = 5;
  const freeCellsCount = width * height - bombCount;
  let bombMap = [];
  let bombNumbers = [];
  let bombCells = [];

  let startGame = cell => {
    console.log('Started!');
    gameStarted = true;
    createBombs(cell.x, cell.y);
  };

  let createBombs = (startX, startY) => {
    bombMap = [];
    bombNumbers = [];
    let count = 0;
    for (let y = 0; y < width; y++) {
      let row = [];
      let rowNum = [];
      for (let x = 0; x < height; x++) {
        row.push(0);
        rowNum.push(0);
      }
      bombMap.push(row);
      bombNumbers.push(rowNum);
    }
    for (let i = 0; i < bombCount; i++) {
      let randX = Math.round(Math.random() * (width - 1));
      let randY = Math.round(Math.random() * (height - 1));
      while (bombMap[randY][randX] || (startX === randX && startY === randY)) {
        randX = Math.round(Math.random() * (width - 1));
        randY = Math.round(Math.random() * (height - 1));

      }
      bombMap[randY][randX] = 1;
    }

    for (let y = 0; y < width; y++) {
      for (let x = 0; x < height; x++) {
        if (bombMap[y][x]) {
          if (x > 0) {
            bombNumbers[y][x - 1]++;
          }
          if (x < width - 1) {
            bombNumbers[y][x + 1]++;
          }
          if (y > 0) {
            bombNumbers[y - 1][x]++;
          }
          if (y < height - 1) {
            bombNumbers[y + 1][x]++;
          }
          if (x < width - 1 & y < height - 1) {
            bombNumbers[y + 1][x + 1]++;
          }
          if (x > 0 & y > 0) {
            bombNumbers[y - 1][x - 1]++;
          }
          if (x > 0 & y < height - 1) {
            bombNumbers[y + 1][x - 1]++;
          }
          if (y > 0 & x < width - 1) {
            bombNumbers[y - 1][x + 1]++;
          }
        }

      }

    }
  }






  mw.style.setProperty('--columns-count', width);
  for (let y = 0; y < width; y++) {
    bombCells[y] = [];
    for (let x = 0; x < height; x++) {
      let mwCell = document.createElement('div');
      mwCell.className = 'mw-cell';
      mwCell.x = x;
      mwCell.y = y;
      bombCells[y][x] = mwCell;
      mw.append(mwCell);
    }
  }

  const gameOver = () => {
    gameStarted = false;
    for (let cell of mw.children) {
      cell.classList.add('mw-cell--open');
      if (bombMap[cell.y][cell.x]) {
        cell.classList.add('mw-cell--bomb')
      }
      else if (bombNumbers[cell.y][cell.x]!=0){
        cell.textContent = bombNumbers[cell.y][cell.x]
      }
    }
    alert('Game over!');
  }

  const gameWin = () => {
    gameStarted = false;
    for (let cell of mw.children) {
      cell.classList.add('mw-cell--open');
      if (bombMap[cell.y][cell.x]) {
        cell.classList.add('mw-cell--bomb')
      }
      else if (bombNumbers[cell.y][cell.x]!=0){
        cell.textContent = bombNumbers[cell.y][cell.x]
      }
    }
    alert('You won!');
  }

  const openCell = (cell, check) => {
    if (!cell) return;
    if (gameStarted && cell.classList.contains('mw-cell--open')) {
      return;
    }
    let x = cell.x;
    let y = cell.y;
    cell.classList.add('mw-cell--open');
    if (bombMap[y][x]) {
      if (!check) {
        cell.classList.add('mw-cell--bomb');
        gameOver();
      } else if (!gameStarted) {
        cell.classList.add('mw-cell--bomb');
      }
    } else if (bombNumbers[y][x]) {
      cell.textContent = bombNumbers[y][x]
      if (gameStarted) {
        return;
      }
    } else {
          if (bombCells[y - 1]) {
            openCell(bombCells[y - 1][x], true);
            openCell(bombCells[y - 1][x - 1], true);
            openCell(bombCells[y - 1][x + 1], true);

          }
          if (bombCells[y + 1]) {
            openCell(bombCells[y + 1][x], true);
            openCell(bombCells[y + 1][x + 1], true);
            openCell(bombCells[y + 1][x - 1], true);

          }
          if (bombCells[y][x - 1]) {
            openCell(bombCells[y][x - 1], true);
          }
          if (bombCells[y][x + 1]) {
            openCell(bombCells[y][x + 1], true);
          }
    }
  }



  let gameStarted = false;
  mw.addEventListener('click', e => {
    const t = e.target;
    if (t.classList.contains('mw-cell')) {
      if (!gameStarted) {
        startGame(t);
      }
      if (t.classList.contains('mw-cell--mark')) {
        return;
      }
      // blurFocus();
      currentCell = t;
      posX = t.x;
      posY = t.y;
      openCell(t);

      let currentFreeCellCount = mw.querySelectorAll('.mw-cell--open').length;
      console.log(freeCellsCount, currentFreeCellCount);
      if (freeCellsCount === currentFreeCellCount) {
        // alert('You won!');
        gameWin();
      }
    }
  });

  const markCell = cell => {
    cell.classList.toggle('mw-cell--mark');
  };

  mw.addEventListener('contextmenu', e => {
    const t = e.target;
    e.preventDefault();
    if (t.classList.contains('mw-cell')) {
      markCell(t);
    }
  });

  let posX = 0;
  let posY = 0;
  let currentCell = bombCells[posY][posX];

  const blurFocus = () => {
    // mw.querySelector('.mw-cell--focus').classList.remove('mw-cell--focus');
    mw.querySelector('.mw-cell--focus').classList.toggle('mw-cell--focus');
  };
  const setFocus = () => {
    // currentCell.classList.add('mw-cell--focus');
    blurFocus();
    currentCell.classList.add('mw-cell--focus');
  }
  const setCurrent = () => {
    currentCell.classList.add('mw-cell--focus');
    if (posY < 0) {posY = height - 1; posX++;}
    if (posY > height - 1) {posY = 0; posX++;}
    if (posX < 0) {posX = width - 1; posY++;}
    if (posX > width - 1) {posX = 0; posY++;}
    currentCell = bombCells[posY][posX];
    // currentCell.classList.add('mw-cell--focus');
    setFocus();
  };

  const moveRight = () => { posX++; setCurrent(); }
  const moveLeft = () => { posX--; setCurrent(); }
  const moveUp = () => { posY--; setCurrent(); }
  const moveDown = () => { posY++; setCurrent(); }

  document.addEventListener('keydown', e => {
    switch (e.code) {
      case 'ArrowUp': moveUp(); break;
      case 'ArrowDown': moveDown(); break;
      case 'ArrowLeft': moveLeft(); break;
      case 'ArrowRight': moveRight(); break;
      case 'Enter':
      case 'Space':
        if (!gameStarted) {
          startGame(currentCell); break;
        }
        if (e.ctrlKey || e.metaKey) {
          markCell(currentCell); break;
        }
        else {
          openCell(currentCell); break;
        }
    }
    let currentFreeCellCount = mw.querySelectorAll('.mw-cell--open').length;
    if (freeCellsCount === currentFreeCellCount) {
      gameStarted = false;
      for (let cell of mw.children) {
        cell.classList.add('mw-cell--open');
        if (bombMap[cell.y][cell.x]) {
          cell.classList.add('mw-cell--bomb')
        }
        else if (bombNumbers[cell.y][cell.x]!=0){
          cell.textContent = bombNumbers[cell.y][cell.x]
        }

      }
      // alert('You won!');
      gameWin();
    }
  });




});
