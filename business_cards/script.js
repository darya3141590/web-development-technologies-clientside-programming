if (document.readyState != 'loading') {
  const form_block = document.querySelector(".card-form");
  const preview_fields = document.querySelectorAll(".preview_field");
  const check_phone = document.querySelector("#check_phone");
	const second_phone_field = document.querySelector("#second_phone_field");
	var check_email = false;
	var check_adress = false;
	var full_name_orientation = "center";
  var full_name_size = "26px";
  var full_name_color_text = "#202020";
	var job_title_orientation = "center";
  var job_title_size = "16px";
	var job_title_color_text = "#202020"

	click_on.addEventListener("click", () => {
		var form_information = form_block.querySelectorAll("input");
    for (var i = 0; i < 2; i++){
			if(form_information[i].value === ""){
				alert("Ваша форма не заполнена.\n Попробуйте еще раз!");
				return;
			}
		}
		for(var i = 2; i < 5; i++){
			if(form_information[i].checked){
				full_name_orientation = form_information[i].value; 
				break;
			}
		}
		for(var i = 5; i < 8; i++){
			if(form_information[i].checked){
				full_name_size = form_information[i].value;
				break;
			}
		}
		for(var i = 8; i < 14; i++){
			if(form_information[i].checked){
				full_name_color_text = form_information[i].value;
				break;
			}
		}
		for(var i = 15; i < 18; i++){
			if(form_information[i].checked){
				job_title_orientation = form_information[i].value;
				break;
			}
		}
		for(var i = 18; i < 21; i++){
			if(form_information[i].checked){
				job_title_size = form_information[i].value;
				break;
			}
		}
		for(var i = 21; i < 27; i++){
			if(form_information[i].checked){
				job_title_color_text = form_information[i].value;
				break;
			}
		}
		if(form_information[31].checked){
			check_email = true;
		}
		if(form_information[33].checked){
			check_adress = true
		}

		preview_fields[0].textContent = form_information[0].value;
		preview_fields[1].textContent = form_information[1].value;
		preview_fields[1].style.textAlign = full_name_orientation;
		preview_fields[1].style.fontSize = full_name_size;
		preview_fields[1].style.color = full_name_color_text;
		preview_fields[2].textContent = form_information[14].value;
		preview_fields[2].style.textAlign = job_title_orientation;
		preview_fields[2].style.fontSize = job_title_size;
		preview_fields[2].style.color = job_title_color_text;
		preview_fields[3].textContent = form_information[27].value;
		preview_fields[4].textContent = form_information[28].value;

		if(check_email){
			preview_fields[5].style.display = "block";
			preview_fields[5].textContent = form_information[30].value;
		}else{
			preview_fields[5].style.display = "none";
		}

		if(check_adress){
			preview_fields[6].style.display = "block";
			preview_fields[6].textContent = form_information[32].value;
		}else{
			preview_fields[6].style.display = "none";
		}
	});

	check_phone.addEventListener('change', (event) => {
  		if (event.currentTarget.checked) {
    		second_phone_field.style.display = "block";
    		document.querySelector("#check_phone + label").style.fontSize = "0";
  		} else {
    		second_phone_field.style.display = "none";
    		second_phone_field.value = "";
    		document.querySelector("#check_phone + label").style.fontSize = "12px";
  		}
	});

}
