function NOD(a, b) {
  if (b == 0) {return a == 1}
  else {return NOD(b, a % b)}
}

function Prime(a, check) {
    for (let i = 2; i <= Math.sqrt(a); i++) {
      if (a % i == 0) {check = false; return check; break;}
    }
    if (check) {return check;}
}

function Prime_to_n(predel, all) {
  let mas = []; let prime_mas = []; let k = 0;
  if (all != 0) {predel = max;}
  for (let i = 2; i < predel; i++) {mas.push(i);}
  for (var i = 2; i <= Math.sqrt(predel); i++) {
        if (mas[i]) {
            for (var j = i * i; j < predel; j += i) {mas[j] = false;}
        }
  }
  for (var i = 2; i < predel; i++) {
      if(mas[i]) {prime_mas.push(i); k+=1;
          if (k == all) {break;}
      }
  }
  return prime_mas;
}

function Dividers(a, all) {
  array = [];
  for (let i = 1; i<=a; i++) {
    if (a % i == 0) {array.push(i);}
  }
  if (all>=0) {
    if (array.length>1) {
      array.pop();
    }
    let result = array.reduce(function(sum, elem) {
       return sum + elem;
    }, 0);
    if (result == a) {return a;}
    else {return 0;}
  }
  else {console.log("1.5: делители  числа: "+a+": ", array);}
}

function punct_1_5_and_1_7(a, all, N) {
  if (all>=0) {
    for (let k = 1; k<=a; k++) {
      let new_a=Dividers(k, all);
      if (new_a!=0) {
        all+=1;
        if (all<=N) {console.log("1.7: число "+new_a+" совершенное");}
        else {break;}
      }
    }
  }
  else {Dividers(a, all, N);}
}

function Fibonachchi(a) {
  let f1=1; let f2=1;
  while (f2<a) {
    let b = f2;
    f2+=f1;
    f1=b;
  }
  if (f2==a) {
    console.log("1.9: число "+a+" Фибоначчи");
  }
  else {
    console.log("1.9: число "+a+" не Фибоначчи");
  }
}

function Pythagorean_triplets(N, M) {
  if (N > M) {
      let b = M;
      M = N;
      N = b;
  }
  console.log("Все различные пифагоровы тройки из интервала от " + N + " до " + M + " :");
  for (let i = N; i <= M; i++) {
    for (let j = N; j <= M; j++) {
      for (let k = N; k <= M; k++) {
          if (i*i+j*j==k*k) {console.log("{ " + i + ";" + j + ";" + k + "}");}
      }
    }
  }
}

function Multiplication_and_division(x1, y1, x2, y2, act) {
  if (act==2) {a=x1*y2; b=y1*x2;}
  else {a=x1*x2; b=y1*y2;}

  let m=Math.min(a,b); let div=1;
  for (let i=1; i<=m; i++) {
    if ((a%i==0) && (b%i==0)) {div=i;}
  }
  console.log("Ответ: " + a/div + "/" + b/div);
}

function Addition_and_substraction(x1, y1, x2, y2, act) {
  if (act==1) {a=(x1*y2)+(x2*y1); b=y1*y2;}
  else {if ((x1*y2) > (x2*y1)) {a=-(x2*y1)-(x1*y2); b=y1*y2;}
  else {a=(x1*y2)-(x2*y1); b=y1*y2;}
  }

  let m=Math.min(Math.abs(a),b); let div=1;
  for (let i=1; i<=m; i++) {
    if ((Math.abs(a)%i==0) && (b%i==0)) {div=i;}
  }
  console.log("Ответ: " + a/div + "/" + b/div);
}

function Check_it(i, d, m) {
  d = i;
  while(d) {
      m = d%10;
      d = Math.floor(d / 10);
      if (i%m) {return false;}
  }
  return true;
}

function Divided_by_each(N, M, d, m) {

  for (let i = N; i <= M; ++i) {
    if (Check_it(i, d, m)) {
      console.log("Все целые числа из интервала от " + N + " до " + M + " , которые делятся на каждую из своих цифр: ", i)
    }
  }
}

function Divided_by_sum(N, M, d, sum, k) {
  k=0;
  for (let i = N; i <= M; ++i) {
    d = i; sum = 0;
    while (d>0) {
      sum = sum + d % 10;
      d = Math.floor(d / 10);
    }
    if (i % sum==0) {
      k = 1;
      console.log("Все целые числа из интервала от " + N + " до " + M + " , которые делятся на сумму всех своих цифр: ", i);
    }
    if (k==0) {console.log('Таких чисел нет');}
  }
}

function Sum_of_three_prime_numbers(N, M) {
  for (let i = N; i <= M; ++i) {
    if (i % 2 == 1) {
      let myprime = []; let prime = [];
      for (let l = 0; l < i; ++l) {
        prime.push(true);
      }
      prime[0] = false; prime[1] = false;
      for (let k = 2; k < i; ++k) {
          if (prime[k]==false) {continue;}
          for (let j = k*k; j < i; j+=k) {prime[j] = false;}
      }
      for (let c = 0; c < i; ++c) {
        if (prime[c]) {myprime.push(c);}
      }
      a = i - 3;
      let res = [3];
      let k = 0;
      while (true) {
        if (a - myprime[k] in myprime) {
          res.push(myprime[k]);
          res.push(a - myprime[k]);
          res.sort();
          console.log(res);
          break;
        }
        k += 1;
      }
    }
    else {continue;}
  }
}

function Multipliers(a) {
  for (let i = 2; i <= a; ++i) {
    if (a%i==0) {
      check = Prime(i, check);
      if (check) {mult.push(i); return Math.floor(a / i);}
      else {continue;}
    }
  }
}

function punct_1_6(a, mult) {
  let old_a = a;
  while (a!=1) {
    a = Multipliers(a, mult);
  }
  let result = {};
  mult.forEach(function(a){
      result[a] = result[a] + 1 || 1;
  });
  console.log("Простые множители числа "+old_a+" и их порядки: ", result);
}

function Double_factorial(N, a, b) {
  let result = 1;
  for (let i = 1; i <= N; i++) {
    result = result * (i * a + b);
  }
  return result;
}

function natur(a, b) {
  if ((a<0) || (b<0)) {alert( "Числа не натуральные" ); return false;}
  else {return true;}
}



let name = alert("Взаимно простые числа");
let a = prompt('Введите a: ', 17); let b = prompt('Введите b: ', 25);
if (natur(a, b)) {
  a = NOD(a, b);
  if (a==1) {console.log("1.1: Числа взаимно простые");}
  else {console.log("1.1: Числа не взаимно простые");}
}

name = alert("Простое ли число?");
a = prompt('Введите a: ', 17); let check = true;
if (natur(a, b=0)) {
  check = Prime(a, check);
  if (check) {console.log("1.2: Число простое");}
  else {console.log("1.2: Число непростое");}
}

name = alert("Все простые числа из интервала 1..N");
let predel = prompt('Введите N: ', 17); let all = 0;
console.log("1.3: все простые числа до "+predel+": ", Prime_to_n(predel, all));


name = alert("Первые N простых чисел");
let max = 9999; all = prompt('Введите N: ', 17);
console.log("1.4: "+all+" простых чисел: ", Prime_to_n(predel, all));

name = alert("Все делители натурального числа N");
a = prompt('Введите N: ', 17); all = -1; let N = 0;
if (natur(a, b=0)) {punct_1_5_and_1_7(a, all, N);}

name = alert("Простые множители числа и их порядки");
a = prompt('Введите N: ', 260); let mult = []; check = true;
punct_1_6(a, mult);

name = alert("Первые N (N<5) совершенных чисел");
a = max; all = 0; N = prompt('Введите N: ', 4);
if (natur(a, b=0)) {punct_1_5_and_1_7(a, all, N);}

name = alert("Нечетные натуральные числа из интервала N..M, которые можно представить в виде суммы трех простых чисел");
N = parseInt(prompt('Введите N от 6 и больше: ', 6)); let M = parseInt(prompt('Введите M: ', 20));
Sum_of_three_prime_numbers(N, M);

name = alert("Число Фибоначчи?");
a = prompt('Введите a: ', 17);
Fibonachchi(a);

name = alert("Вычисление (N)!!");
a = 2; b = 1; N = prompt('Введите N: ', 14);
console.log("("+N+")!! = ", Double_factorial(N, a, b));

name = alert("Различные пифагоровы тройки из интервала от N до М");
N = prompt('Введите N: ', 1); M = prompt('Введите M: ', 20);
Pythagorean_triplets(N, M);

name = alert("Программа умножения (деления) двух данных рациональных чисел");
let x1 = prompt('Введите числитель первой дроби: ', 1); let y1 = prompt('Введите знаменатель первой дроби: ', 2); let x2 = prompt('Введите числитель второй дроби: ', 3); let y2 = prompt('Введите знаменатель второй дроби: ', 4);
let act = prompt('Выберите действие: умножить - 1, делить - 2', 1);
Multiplication_and_division(x1, y1, x2, y2, act);

name = alert("Программа сложения (вычитания) двух данных рациональных чисел");
x1 = prompt('Введите числитель первой дроби: ', 1); y1 = prompt('Введите знаменатель первой дроби: ', 2); x2 = prompt('Введите числитель второй дроби: ', 3); y2 = prompt('Введите знаменатель второй дроби: ', 4);
act = prompt('Выберите действие: сложить - 1, вычесть - 2', 1);
Addition_and_substraction(x1, y1, x2, y2, act);

name = alert("Все целые числа из интервала от N до M, которые делятся на каждую из своих цифр");
N = prompt('Введите N: ', 1); M = prompt('Введите M: ', 20); let d=0; let m=0;
Divided_by_each(N, M, d, m);

name = alert("Все целые числа из интервала от N до M, которые делятся на сумму всех своих цифр");
N = prompt('Введите N: ', 1); M = prompt('Введите M: ', 20); let sum=0; let k=0;
Divided_by_sum(N, M, d, sum, k);
