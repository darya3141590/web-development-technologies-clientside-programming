function camelize(str) {
    let arr = str.split('-');
    arr.forEach((item) => {
        arr[arr.indexOf(item)] = item.charAt(0).toUpperCase() + item.slice(1, item.length);
    });
    str2 = arr.join('');
    return str2.charAt(0).toLowerCase() + str2.slice(1, str2.length);
}

function first_reverse_order(first_clone) {
  let len = array.length
  for (let i = 0; i <=len/2; i++) {
       let k  =  first_clone[len-1-i];
       first_clone[len-i]=first_clone[i];
       first_clone[i] = k;
  }
  first_clone.splice(Math.floor(len/2), 1);
  return first_clone;
}

function second_reverse_order(second_clone) {
  let new_array = [];
  for (let i = second_clone.length-1; i>=0 ; i--) {
      new_array.push(second_clone[i]);
  }
  return new_array;
}

function getWeekDay(week, now) {
  return week[now.getDay()];
}

function getAbbreviatedWeekDay(week, now) {
  return week[now.getDay()+7];
}

function new_names(personal2) {
  personal2["archaeologist"] = "Ivan";
  personal2["journalist"] = "Sonya";
  personal2["chef"] = "Frederico";
  prompt("Посмотрите в консоль", personal2);
};

function other_language(phraze, korean) {
  let sp = phraze.split(" ");
  for (i in sp) {
    sp[i] = korean[sp[i]];
  }
  let str = sp.join(' ');
  return str;
};

function add_new_sub(subjects, sub, subjects_array) {
  console.log(subjects_array.indexOf(sub));
  if (subjects_array.indexOf(sub) != -1) {
    subjects_array.splice(subjects_array.indexOf(sub), 1);
  } else {
    subjects_array.push(sub);
  }
  subjects["your_sub"] = subjects_array.join(", ");
  console.log(subjects);
};

console.log("2.1:");
let phraze = prompt("Введите фразу");
console.log(camelize(phraze));

console.log("2.2:");
let array = [9,8,7,6,5,4,3,2,1];
let first_clone = array.slice(0);
let second_clone = Object.assign([], array);
console.log(array, first_reverse_order(first_clone), second_reverse_order(second_clone));

console.log("2.3:");
let korean = {
  Я: "저는",
  Даша: "다샤에요.",
  приятно: "만나서",
  познакомиться: "반갑습니다!",
};
phraze = "Я Даша приятно познакомиться";
console.log(other_language(phraze, korean));

console.log("2.4:");
let week = {
7: 'Sunday',
1: 'Monday',
2: 'Tuesday',
3: 'Wednesday',
4: 'Thursday',
5: 'Friday',
6: 'Saturday'
};
let now = new Date();
console.log(getWeekDay(week, now));

console.log("2.5:");
let personal = {
archaeologist: "Michael",
journalist: "Rita",
chef: "George",
toString() {
  console.log(`archaeologist ${this.archaeologist}\n journalist ${this.journalist}\n chef ${this.chef}\n`);
}
};
prompt("Посмотрите в консоль", personal)
let personal2 = Object.assign({}, personal);
new_names(personal2);

console.log("2.6:");
let days = "Mon, Tue, Wed, Thu, Fri, Sat, Sun";
let days_array = days.split(", ");
for (let i = 0; i <days_array.length; i++) {
  week[i+8] = days_array[i];
};
console.log(getAbbreviatedWeekDay(week, now));

console.log("2.7:");
let subjects = {
"your_sub": "Math, PE, Design, IT developing"
};
let subjects_array = subjects["your_sub"].split(", ");
let sub = prompt("Введите предмет на английском:", "animation");
add_new_sub(subjects, sub, subjects_array);
